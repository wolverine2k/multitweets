import './polyfills.ts';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { environment } from './environments/environment';
import { AppModule } from './app/app.module';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule);

/*
import { FIREBASE_PROVIDERS, defaultFirebase, AngularFire, 
          firebaseAuthConfig, AuthProviders, AuthMethods } from 'angularfire2';
import { ROUTER_DIRECTIVES } from '@angular/router';

bootstrap(AppComponent, [
  FIREBASE_PROVIDERS,
  // Initialize Firebase app
  defaultFirebase({
    apiKey: environment.defaultFirebase.apiKey,
    authDomain: environment.defaultFirebase.authDomain,
    databaseURL: environment.defaultFirebase.databaseURL,
    storageBucket: environment.defaultFirebase.storageBucket
  }),
  ROUTER_DIRECTIVES,
  firebaseAuthConfig({
    provider: AuthProviders.Twitter,
    method: AuthMethods.Popup
  })
]);
*/