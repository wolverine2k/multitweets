// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

import { AuthProviders, AuthMethods } from 'angularfire2';

export const environment = {
  production: false,
  defaultFirebase: {
    apiKey: "AIzaSyDcdG-GL1dGxkj0o5iCvRIxyzKvUf8Dcvo",
    authDomain: "multitweet-b2f1f.firebaseapp.com",
    databaseURL: "https://multitweet-b2f1f.firebaseio.com",
    storageBucket: "multitweet-b2f1f.appspot.com"
  },
  defaultFirebaseAuthConfig: {
    provider: AuthProviders.Twitter,
    method: AuthMethods.Redirect
  },
  defaultTwitterOAuth: {
    consumerKey: "1XP1rlHPlMGHoiDguYw7hgwsm"
  }
};