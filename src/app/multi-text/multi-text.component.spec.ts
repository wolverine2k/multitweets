/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { addProviders, async, inject } from '@angular/core/testing';
import { MultiTextComponent } from './multi-text.component';
import { TwitterService } from './../twitter.service';

describe('Component: MultiText', () => {
  beforeEach(() => {
    addProviders([TwitterService]);
  });
  
  it('should create an instance', () => {
    inject([TwitterService], (component: MultiTextComponent) => {
      expect(component).toBeTruthy();
    });
  });
});
