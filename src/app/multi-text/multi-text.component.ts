import { Component, OnInit } from '@angular/core';
import { TwitterService } from './../twitter.service';

@Component({
  selector: 'app-multi-text',
  templateUrl: './multi-text.component.html',
  styleUrls: ['./multi-text.component.css'],
  providers: [TwitterService]
})
export class MultiTextComponent implements OnInit {
  private _longText: string = "";
  private _smallText: Array<string>;
  private _combinedText: Array<string>;
  private _multiTweets: Array<string>;

  private _tweetLength: number = 130;
  private _totalTweets: number = 0;
  private _promotionText: string = "\nTweeted with MultiTweets - https://goo.gl/zeM0rS";

  constructor(private ts: TwitterService) { }

  ngOnInit() {
    this._combinedText = new Array<string>();
    this._multiTweets = new Array<string>();
  }

  fireOffTweets() {
    if (this._multiTweets.length === 0) {
      return;
    }
    this.ts.postMyTweets(this._multiTweets).subscribe(
      /* Should be showing a modal dialog here...*/
      data => {console.log(data)},
      error => {console.log("ERROR SENDING TWEETS")}
    );
  }

  prepareTweets(newValue: string) {
    if (newValue.length === 0) {
      this._totalTweets = 0;
      return;
    }

    let promotedText: string = newValue + this._promotionText;
    this._longText = newValue;
    this._totalTweets = Math.floor((promotedText.length) / this._tweetLength) + 1;

    let count: number = 0;
    let tweetCount: number = 0;
    let i: number = 0;

    this._combinedText = new Array<string>();
    this._multiTweets = new Array<string>();
    this._smallText = new Array<string>();
    this._smallText = promotedText.split(" ");

    if (this._totalTweets === 1) {
      this._multiTweets.push(promotedText);
    } else {
      for (i = 0; i < this._smallText.length; ++i) {
        if ((count + (this._smallText[i].length) + 1) < this._tweetLength) {
          this._combinedText.push(this._smallText[i]);
          count += (this._smallText[i].length + 1);
        } else {
          if (tweetCount < (this._totalTweets - 1)) {
            tweetCount += 1;
            this._combinedText.push("(" + tweetCount + "/" + this._totalTweets + ")");
            this._multiTweets.push(this._combinedText.join(" "));
            count = this._smallText[i].length + 1;
            this._combinedText = new Array<string>();
            this._combinedText.push(this._smallText[i]);
          }
        }
        if (tweetCount === (this._totalTweets - 1)) {
          tweetCount += 1;
          this._smallText = this._smallText.slice(i);
          let tempString: string = "";
          tempString = this._smallText.join(" ");
          tempString += "(" + tweetCount + "/" + this._totalTweets + ")";
          this._multiTweets.push(tempString);
          break;
        }
      }
    }
  }

}
