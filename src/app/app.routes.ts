import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';

RouterModule.forRoot([
  {
    path: 'app',
    component: AppComponent
  }
])