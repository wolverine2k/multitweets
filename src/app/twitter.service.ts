import { Injectable } from '@angular/core';
import { AngularFire } from 'angularfire2';
import { Observable } from 'rxjs/Observable';
//import * as codebird from 'codebird';

//import { Http, Jsonp } from '@angular/http';
//import { OAuth } from 'oauth/index.js';

declare var Codebird: any;

@Injectable()
export class TwitterService {
  private _url:string = "https://api.twitter.com/1.1/statuses/update.json?status=";
  private _authorizationData: string = "";
  public _authData: any = null;
  public _twitterSecrets: any = null;
  private _cb;

  constructor() {
    this._cb = new Codebird;
    this._cb.setConsumerKey("1XP1rlHPlMGHoiDguYw7hgwsm", "jIZF3CQW43fxfgLQZ4WsLrLb8H4DKCe16MMZtu5nMH8kMP8cgL");
  }

  sendTweetsATime(tweet: string): boolean {
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.open('POST', this._url+tweet, true);
    xhr.setRequestHeader("Authorization", this._authorizationData);
    xhr.send();
    /*
    let authHeader:string = "OAuth oauth_consumer_key"
    xhr.setRequestHeader('Authorization', "apiKey","AIzaSyDcdG-GL1dGxkj0o5iCvRIxyzKvUf8Dcvo");*/

/*

status	Hello Ladies + Gentlemen, a signed OAuth request!
include_entities	true
oauth_consumer_key	xxx
oauth_nonce	yyy
oauth_signature_method	HMAC-SHA1
oauth_timestamp	1318622958
oauth_token	abc
oauth_version	1.0

Consumer Secret (API Secret)	zzz

http://oauthbible.com/: AccessToken same as OAUTHToken
oauth_token: Users
*/
    return false;
  }

  postMyTweets(tweets: Array<string>) {
    console.log("Tweeting...");
    //this._cb.setToken(this._twitterSecrets.)
    return (Observable.create(observer => {
      setTimeout(() => {
        for(var tweet of tweets) {
          if(this.sendTweetsATime(tweet) === false) {
            observer.error("ERROR sending the tweet... " + tweet);
            break;
          }
        }
        observer.next("All done again...");
        observer.complete();
      }, 1);
    }));
  }

}
