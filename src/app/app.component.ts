import { Component } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import * as firebase from 'firebase';
import { TwitterService } from './twitter.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [TwitterService]  
})
export class AppComponent {
  private _angularFire: AngularFire = null;
  private _loggedIn: boolean;

  constructor(af: AngularFire, ts: TwitterService) {
    this._angularFire = af;
    this._angularFire.auth.subscribe(
      auth => {
        if(!auth)
          return;
        ts._authData = JSON.stringify(auth);
        console.debug("Am trying to write a function");
        console.debug(ts._authData);
        ts._twitterSecrets = auth.twitter;
        console.debug(JSON.stringify(ts._twitterSecrets));
        //let users = this._angularFire.database.object('users/' + auth.uid);
        let users = firebase.database().ref().child('users').child(auth.uid);
        users.update({
          "uid": auth.auth.uid,
          "authData": ts._authData,
          "displayName": auth.auth.displayName,
          "profileImageURL": auth.auth.photoURL
        });
      },
      error => console.log(error)
    );
  }

  doLogin() {
    this._angularFire.auth.login();
    this._loggedIn = true;
  }

  doLogout() {
    this._loggedIn = false;
    this._angularFire.auth.logout();
  }
}
