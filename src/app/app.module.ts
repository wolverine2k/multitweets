import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }   from './app.component';
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';
import { environment } from '../environments/environment';
import { FooterComponent } from '../ui/footer';
import { HeaderComponent } from '../ui/header';
import { MultiTextComponent } from './multi-text';

/*
import { FIREBASE_PROVIDERS, defaultFirebase, AngularFire, 
          firebaseAuthConfig, AuthProviders, AuthMethods } from 'angularfire2';
*/

@NgModule({
  imports:      [ 
    BrowserModule,
    AngularFireModule.initializeApp(environment.defaultFirebase, environment.defaultFirebaseAuthConfig)
  ],
  declarations: [ AppComponent, FooterComponent, HeaderComponent, MultiTextComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }