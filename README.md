# Multitweets README

**Multitweets** divide your long tweets into a series of multiple tweets numbered in x/n fashion where x is the tweet number out of a total of n number of tweets! Using Firebase with Twitter Auth hosted on 

[https://multitweet-b2f1f.firebaseapp.com/](https://multitweet-b2f1f.firebaseapp.com/)

### (c) Naresh Mehta [https://www.naresh.se/](https://www.naresh.se/) 2016

Code hosted on [https://bitbucket.org/wolverine2k/Multitweets](https://bitbucket.org/wolverine2k/multitweets)

Check more of my repositories on:
 1. [https://bitbucket.org/wolverine2k](https://bitbucket.org/wolverine2k)
 2. [https://github.com/wolverine2k](https://github.com/wolverine2k)
 3. [http://codepen.io/wolverine2k/](http://codepen.io/wolverine2k/)
 4. [http://plnkr.co/users/wolverine2k](http://plnkr.co/users/wolverine2k)

I am also on twitter [https://twitter.com/wolverine2k](https://twitter.com/wolverine2k).

# 

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.10.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

# 

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Development Server with live-reload
Run `ng serve --port <port> --live-reload-port 49153` for a dev server with live-reload. Navigate to `http://localhost:<port>/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/route/class`.

## Compile & test
Run `npm run build` and `npm start` for running locally.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

### Below are equivalent for production
  * `ng build --target=production --environment=prod`
  * `ng build --prod --env=prod`
  * `ng build --prod`
### Below are equivalent for development
  * `ng build --target=development --environment=dev`
  * `ng build --dev --e=dev`
  * `ng build --dev`
  * `ng build`

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/). 
Before running the tests make sure you are serving the app via `ng serve`.

## Deploying to Github Pages

Run `ng github-pages:deploy` to deploy to Github Pages.

## Further help

To get more help on the `angular-cli` use `ng --help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md). Angular-cli npm package documentation available at [Angular-CLI](https://www.npmjs.com/package/angular-cli).
