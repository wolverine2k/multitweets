import { MultitweetsPage } from './app.po';

describe('multitweets App', function() {
  let page: MultitweetsPage;

  beforeEach(() => {
    page = new MultitweetsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
